import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {StatusBar, Platform} from 'react-native';
import AuthContext from './src/context/AuthContext';
import StackNavigator from './src/navigation/StackNavigator';

const App = () => {
  const [isLogIn, setIsLogin] = useState(null);

  // Auth Context Value
  const authValue = {
    signIn: async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        console.log('token: ', token);
        if (token !== null) {
          setIsLogin(token);
        }
      } catch (error) {
        console.log('error login guys', error);
      }
    },
    signOut: async () => {
      try {
        await AsyncStorage.removeItem('token');
        setIsLogin(null);
      } catch (error) {
        console.log('error logout gays', error);
      }
    },
    token: isLogIn,
  };

  // Get Stored Token
  const getStoredToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {
        setIsLogin(token);
      }
    } catch (error) {
      console.log('gagal ngambil stored token', error);
    }
  };

  useEffect(() => {
    getStoredToken();
  }, []);

  return (
    <AuthContext.Provider value={authValue}>
      <NavigationContainer>
        <StatusBar
          backgroundColor="#FD4D4D"
          barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
          hidden={false}
        />
        <StackNavigator tokenLogin={isLogIn} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;
