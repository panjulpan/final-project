import {
  StyleSheet,
  Text,
  SafeAreaView,
  ImageBackground,
  View,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import TopBarDetail from '../../components/TopBarDetail';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconTwo from 'react-native-vector-icons/Feather';

const DetailProductScreen = ({navigation, route}) => {
  const {data} = route.params;
  const [wishlist, setWishlist] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={{uri: data.image}}
        style={styles.imageContainer}
        imageStyle={styles.imageStyle}>
        <TopBarDetail onPress={() => navigation.goBack()} />
      </ImageBackground>
      <View style={styles.contentContainer}>
        <View style={styles.topContentContainer}>
          <View style={styles.topContentLeft}>
            <Text style={styles.contentTitle}>{data.title}</Text>
            <View style={styles.subContentContainer}>
              <Text style={styles.subContentPrice}>$ {data.price}</Text>
              <View style={styles.subContentPromoContainer}>
                <Text style={styles.subContentPromo}>Free Antiseptic</Text>
              </View>
            </View>
          </View>
          <View style={styles.topContentRight}>
            <TouchableOpacity onPress={() => setWishlist(!wishlist)}>
              <Icon
                name="heart"
                size={22}
                color={wishlist ? '#FD4D4D' : '#18203A'}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bottomContentContainer}>
          <View style={styles.bottomContentLeftContainer}>
            <View style={styles.bottomContentLeft}>
              <IconTwo name="star" color="#FD4D4D" size={18} />
              <Text style={styles.bottomContentLeftRate}>
                {data.rating.rate} / 5
              </Text>
            </View>
            <View style={styles.bottomContentCountContainer}>
              <Text style={styles.bottomContentCount}>
                {data.rating.count} Sold / day
              </Text>
            </View>
          </View>
          <View style={styles.bottomContentRightContainer}>
            <IconTwo name="truck" color="#FD4D4D" size={18} />
            <Text style={styles.bottomContentRight}>Free Delivery</Text>
          </View>
        </View>
      </View>
      <View style={styles.bottomButtonContainer}>
        <TouchableOpacity style={styles.buttonOne}>
          <IconTwo name="message-square" size={22} color="#00D68F" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonTwo}>
          <Text style={styles.buttonTwoText}>Beli Sekarang</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonThree}>
          <IconTwo name="shopping-cart" size={22} color="#FFFFFF" />
          <Text style={styles.buttonThreeText}>+ Keranjang</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default DetailProductScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {width: '100%', height: '55%'},
  imageStyle: {width: '100%', height: '100%', resizeMode: 'cover'},
  contentContainer: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    width: '100%',
  },
  topContentContainer: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  topContentLeft: {flex: 0.9},
  contentTitle: {
    fontSize: 14,
    fontWeight: '600',
    color: '#222B45',
    marginBottom: 5,
  },
  subContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subContentPrice: {fontSize: 18, fontWeight: '600', color: '#FF3D71'},
  subContentPromoContainer: {
    borderWidth: 1,
    borderColor: '#FF3D71',
    padding: 3,
    borderRadius: 5,
    marginLeft: 10,
  },
  subContentPromo: {fontSize: 10, fontWeight: '400', color: '#FF3D71'},
  topContentRight: {flex: 0.1},
  bottomContentContainer: {
    paddingHorizontal: 15,
    paddingVertical: 7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottomContentLeftContainer: {flexDirection: 'row'},
  bottomContentLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRightColor: '#828282',
    borderRightWidth: 1,
    paddingRight: 10,
    marginRight: 10,
  },
  bottomContentLeftRate: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4F4F4F',
    marginLeft: 5,
  },
  bottomContentCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomContentCount: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4F4F4F',
  },
  bottomContentRightContainer: {flexDirection: 'row', alignItems: 'center'},
  bottomContentRight: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4F4F4F',
    marginLeft: 5,
  },
  bottomButtonContainer: {
    position: 'absolute',
    width: '100%',
    backgroundColor: '#FFFFFF',
    bottom: 0,
    paddingHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 25,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  buttonOne: {
    borderWidth: 1,
    borderColor: '#00D68F',
    padding: 5,
    borderRadius: 8,
  },
  buttonTwo: {
    borderWidth: 1,
    borderColor: '#FF3D71',
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
  buttonTwoText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#FF3D71',
  },
  buttonThree: {
    flexDirection: 'row',
    backgroundColor: '#FF3D71',
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
  buttonThreeText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    marginLeft: 10,
  },
});
