import {
  StyleSheet,
  Text,
  SafeAreaView,
  Image,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Icon from 'react-native-vector-icons/Feather';
import IconTwo from 'react-native-vector-icons/MaterialCommunityIcons';
import IconThree from 'react-native-vector-icons/FontAwesome';
import TopBarHome from '../../components/TopBarHome';
import Menu from '../../components/Menu';
import {GetProductByLimit} from '../../services/ProductServices';
import ProductComponent from '../../components/ProductComponent';
import {NavigationContainer} from '@react-navigation/native';

const Home = ({navigation}) => {
  const [product, setProduct] = useState([]);

  const getProduct = async () => {
    const prod = await GetProductByLimit();
    if (prod.status === 200 || prod.status === 201) {
      console.log(prod.data);
      setProduct(prod.data);
    } else {
      console.log(prod);
    }
  };

  const renderItem = ({item}) => {
    return (
      <ProductComponent
        item={item}
        onPress={() => navigation.navigate('Detail', {data: item})}
      />
    );
  };

  useEffect(() => {
    getProduct();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <Image
          source={require('../../../assets/images/Banner.png')}
          style={styles.imageBackground}
        />
        <TopBarHome />
        <View style={styles.contentContainer}>
          <View style={styles.menuContainer}>
            <TouchableOpacity style={styles.qrButton}>
              <IconTwo name="qrcode-scan" size={28} color="#4F4F4F" />
            </TouchableOpacity>
            <View style={styles.saldoCouponContainer}>
              <IconThree name="dollar" size={28} color="#4F4F4F" />
              <View>
                <Text style={styles.saldoCouponTitle}>Rp. 1.055.000</Text>
                <TouchableOpacity>
                  <Text style={styles.saldoCouponSub}>Top Up</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.verticalLine} />
            <View style={styles.saldoCouponContainer}>
              <IconTwo name="ticket-percent" size={28} color="#4F4F4F" />
              <View>
                <Text style={styles.saldoCouponTitle}>9 Kupon Baru</Text>
                <TouchableOpacity>
                  <Text style={styles.saldoCouponSub}>Kupon Saya</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              justifyContent: 'space-between',
              width: 100 * 4.2,
            }}>
            <Menu title="Lihat Semua" color="#2F80ED" icon="grid" />
            <Menu title="Gratis Ongkir Xtra" color="#F2C94C" icon="truck" />
            <Menu title="TopUp & Tagihan" color="#EB5757" icon="repeat" />
            <Menu title="Deals Di Sekitar" color="#F2994A" icon="map-pin" />
            <Menu title="Cashback & Voucher" color="#27AE60" icon="bookmark" />
            <Menu title="Travel Entertain" color="#9B51E0" icon="send" />
          </ScrollView>
        </View>
        <Image
          source={require('../../../assets/images/promo.png')}
          style={styles.bannerPromotion}
        />
        <View style={styles.prodList}>
          <View style={styles.prodListTop}>
            <View style={styles.prodListTopContainer}>
              <Text style={styles.prodListTitle}>Flash Sale</Text>
              <Text style={styles.prodListSubTitle}>03.03.30</Text>
            </View>
            <TouchableOpacity style={styles.prodListTopContainer}>
              <Text style={styles.prodListButtonText}>All</Text>
              <Icon name="chevron-right" size={14} color="#FF2D55" />
            </TouchableOpacity>
          </View>
          <FlatList
            data={product}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={styles.prodList}>
          <View style={styles.prodListTop}>
            <View style={styles.prodListTopContainer}>
              <Text style={styles.prodListTitle}>Popular Product</Text>
            </View>
            <TouchableOpacity style={styles.prodListTopContainer}>
              <Text style={styles.prodListButtonText}>All</Text>
              <Icon name="chevron-right" size={14} color="#FF2D55" />
            </TouchableOpacity>
          </View>
          <FlatList
            data={product.sort()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  scrollView: {height: '100%'},
  imageBackground: {
    width: '100%',
    resizeMode: 'cover',
  },
  contentContainer: {
    backgroundColor: '#255BA5',
    width: '100%',
    // height: '57%',
    padding: 10,
  },
  menuContainer: {
    backgroundColor: '#F4F4F4',
    flexDirection: 'row',
    borderRadius: 8,
    marginBottom: 10,
  },
  qrButton: {
    backgroundColor: '#E4E4E4',
    padding: 12,
    borderRadius: 8,
  },
  saldoCouponContainer: {
    flexDirection: 'row',
    flex: 0.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  saldoCouponTitle: {fontSize: 13, color: '#333333', fontWeight: '400'},
  saldoCouponSub: {fontSize: 10, color: '#FF2D55', fontWeight: '700'},
  verticalLine: {
    height: '70%',
    width: 1,
    backgroundColor: '#828282',
    alignSelf: 'center',
  },
  bannerPromotion: {
    resizeMode: 'cover',
    alignSelf: 'center',
    width: '100%',
    marginBottom: 10,
  },
  prodList: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    borderRadius: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  prodListTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  prodListTopContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  prodListTitle: {
    fontSize: 18,
    fontWeight: '800',
    color: '#FF2D55',
    marginRight: 10,
  },
  prodListSubTitle: {
    fontSize: 12,
    fontWeight: '500',
    color: '#333333',
  },
  prodListButtonText: {
    fontSize: 14,
    fontWeight: '500',
    color: '#828282',
  },
});
