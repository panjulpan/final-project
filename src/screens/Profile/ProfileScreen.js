import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useState, useContext} from 'react';
import TopBarProfile from '../../components/TopBarProfile';
import TitleBar from '../../components/TitleBar';
import ButtonProfile from '../../components/ButtonProfile';
import MenuProfile from '../../components/MenuProfile';
import Icon from 'react-native-vector-icons/Feather';
import AuthContext from '../../context/AuthContext';

const ProfileScreen = () => {
  const [akun, setAkun] = useState('Pembeli');
  const {signOut} = useContext(AuthContext);

  const logout = async () => {
    Alert.alert('Perhatian!', 'Anda yakin akan keluar dari akun ini?', [
      {text: 'Ya', onPress: () => signOut()},
      {text: 'Tidak', style: 'destructive'},
    ]);
  };

  return (
    <SafeAreaView style={styles.container}>
      <TopBarProfile />
      <ScrollView showsVerticalScrollIndicator={false}>
        <TitleBar title="Moch. Fadli Rahmadi" subtitle="Silver Member" />
        <View style={styles.topContentContainer}>
          <View style={styles.topContentFirstContainer}>
            <Image
              source={require('../../../assets/images/oval.png')}
              style={styles.imageContent}
            />
            <View style={styles.contentFirstContainer}>
              <Text style={styles.contentFirst}>Follower: 4</Text>
              <Text style={styles.contentFirst}>Following: 31</Text>
            </View>
          </View>
          <View style={styles.topContentSecondContainer}>
            <Text style={styles.contentSecondTitle}>Kupon Saya</Text>
            <Text style={styles.contentSecond}>9 Kupon Baru</Text>
          </View>
          <View style={styles.topContentSecondContainer}>
            <View style={styles.contentSecondTitleContainer}>
              <Text style={styles.contentSecondTitle}>Top Up</Text>
              <Text style={styles.contentSecondTitleBlack}>Saldo {'>'}</Text>
            </View>
            <Text style={styles.contentSecond}>Rp. 1.055.000</Text>
          </View>
        </View>
        <View style={styles.buttonContentContainer}>
          <TouchableOpacity
            style={
              akun === 'Pembeli'
                ? styles.buttonContentPick
                : styles.buttonContent
            }
            onPress={() => setAkun('Pembeli')}>
            <Text>Akun Pembeli</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              akun === 'Toko' ? styles.buttonContentPick : styles.buttonContent
            }
            onPress={() => setAkun('Toko')}>
            <Text>Akun Toko</Text>
          </TouchableOpacity>
        </View>
        {akun === 'Pembeli' ? (
          <>
            <TitleBar title="Transaksi" subtitle="Lihat Riwayat Transaksi >" />
            <View style={styles.contentContainer}>
              <ButtonProfile
                title="Menunggu Pembayaran"
                subtitle="Semua transaksi yang belum dibayar"
              />
              <View style={styles.menuContainer}>
                <MenuProfile
                  color="#FF2D55"
                  icon="credit-card"
                  title="Top-Up & Tagihan"
                />
                <MenuProfile
                  color="#FF2D55"
                  icon="send"
                  title="Tiket & Pesawat"
                />
                <MenuProfile color="#FF2D55" icon="inbox" title="Dikemas" />
                <MenuProfile color="#FF2D55" icon="truck" title="Dikirim" />
                <MenuProfile
                  color="#FF2D55"
                  icon="star"
                  title="Beri Penilaian"
                />
              </View>
              <ButtonProfile
                title="Komplain Sebagai Pembeli"
                subtitle="Lihat status komplain"
              />
            </View>
            <Text style={styles.favoriteContainer}>Favorit Saya</Text>
            <View style={styles.contentContainer}>
              <ButtonProfile
                title="Terakhir dilihat"
                subtitle="Cek produk terakhir yang dilihat"
              />
              <ButtonProfile
                title="Wishlist"
                subtitle="Cek produk yang  anda wishlist"
              />
              <ButtonProfile
                title="Toko Favorit"
                subtitle="Lihat toko yang anda ikuti"
              />
              <ButtonProfile
                title="Langganan"
                subtitle="Lihat toko langganan"
              />
            </View>
            <TouchableOpacity style={styles.logoutButton} onPress={logout}>
              <Icon name="log-out" color="#FF2D55" size={24} />
              <Text style={styles.logoutButtonText}>Keluar</Text>
            </TouchableOpacity>
          </>
        ) : null}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  topContentContainer: {
    paddingHorizontal: 15,
    paddingVertical: 7,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    width: '100%',
  },
  topContentFirstContainer: {flexDirection: 'row', alignItems: 'center'},
  imageContent: {height: 42, width: 42, resizeMode: 'cover'},
  contentFirstContainer: {marginLeft: 10},
  contentFirst: {
    fontSize: 10,
    fontWeight: '400',
    color: '#8F9BB3',
    lineHeight: 11.5,
  },
  topContentSecondContainer: {justifyContent: 'center'},
  contentSecondTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentSecondTitle: {
    fontSize: 10,
    fontWeight: '700',
    color: '#FF2D55',
    marginBottom: 3,
  },
  contentSecondTitleBlack: {
    fontSize: 10,
    fontWeight: '600',
    color: '#333333',
  },
  contentSecond: {
    fontSize: 13,
    fontWeight: '400',
    color: '#333333',
  },
  buttonContentContainer: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginBottom: 10,
  },
  buttonContentPick: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FF3D71',
    borderBottomWidth: 2,
    flex: 0.5,
    padding: 10,
  },
  buttonContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.5,
    padding: 10,
  },
  contentContainer: {backgroundColor: '#FFFFFF', borderRadius: 10},
  menuContainer: {
    paddingHorizontal: 15,
    paddingVertical: 8,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  favoriteContainer: {
    fontSize: 18,
    fontWeight: '600',
    color: '#222B45',
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 7,
  },
  logoutButton: {
    marginVertical: 20,
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  logoutButtonText: {
    marginLeft: 15,
    fontSize: 16,
    fontWeight: '400',
    color: '#FF2D55',
  },
});
