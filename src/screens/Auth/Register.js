import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import TopBar from '../../components/TopBar';

const Register = ({navigation}) => {
  const [keyboardStat, setKeyboardStat] = useState(false);

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStat(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStat(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/let-start.png')}
        style={styles.imageBackground}
        imageStyle={styles.imageStyle}>
        <TopBar onPress={() => navigation.goBack()} />
        <View style={styles.titleContainer}>
          <Text style={styles.welcomeText}>Lets Start</Text>
        </View>
        <ScrollView
          style={
            keyboardStat ? styles.loginContainerKeyboard : styles.loginContainer
          }>
          <Text style={styles.loginTitle}>Sign Up</Text>
          <View style={styles.formContainer}>
            <View>
              <TextInput
                placeholder="Name"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
              />
              <TextInput
                placeholder="Email"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
              />
              <TextInput
                placeholder="Phone Number"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
              />
              <TextInput
                placeholder="Password"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
              />
              <View style={styles.bottomContainer}>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                  <Text style={styles.forgotPassword}>
                    Already have an account ?
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={styles.signIn}>Sign Up</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  imageBackground: {
    height: '100%',
    width: '100%',
  },
  imageStyle: {
    width: '100%',
    height: '55%',
    resizeMode: 'cover',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingHorizontal: 30,
  },
  welcomeText: {
    fontSize: 48,
    color: '#FFFFFF',
    fontWeight: '400',
  },
  loginContainer: {
    backgroundColor: '#FFFFFF',
    padding: 25,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '55%',
  },
  loginContainerKeyboard: {
    backgroundColor: '#FFFFFF',
    padding: 25,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '55%',
  },
  loginTitle: {
    fontSize: 36,
    color: '#18203A',
    fontWeight: '600',
    marginVertical: 10,
  },
  formContainer: {
    justifyContent: 'space-between',
    height: '100%',
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#828282',
    paddingVertical: 5,
    marginTop: 5,
    marginBottom: 25,
  },
  bottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  forgotPassword: {
    fontSize: 15,
    color: '#FD4D4D',
    fontWeight: '600',
  },
  signIn: {
    fontSize: 15,
    color: '#231F20',
    fontWeight: '600',
  },
});
