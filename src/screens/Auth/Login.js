import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Alert,
  Keyboard,
} from 'react-native';
import React, {useState, useContext, useEffect} from 'react';
import TopBar from '../../components/TopBar';
import AuthContext from '../../context/AuthContext';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
  const {signIn} = useContext(AuthContext);
  const [loginData, setLoginData] = useState({
    username: '',
    password: '',
  });
  const [keyboardStat, setKeyboardStat] = useState(false);

  const login = async () => {
    if (
      loginData.username === 'finalProject@mail.com' &&
      loginData.password === 'finalproject123'
    ) {
      await AsyncStorage.setItem('token', 'ASdaswwdfQWDEASDFf');
      await signIn();
    } else {
      Alert.alert('Perhatian', 'Email/Password yang anda masukan salah.');
    }
  };

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStat(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStat(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/welcome-back.png')}
        style={styles.imageBackground}
        imageStyle={styles.imageStyle}>
        <TopBar onPress={() => navigation.goBack()} />
        <View style={styles.titleContainer}>
          <Text style={styles.welcomeText}>Welcome {'\n'}Back</Text>
        </View>
        <ScrollView
          style={
            keyboardStat
              ? styles.loginContainerKeyboardShow
              : styles.loginContainer
          }>
          <Text style={styles.loginTitle}>Sign In</Text>
          <View style={styles.formContainer}>
            <View>
              <TextInput
                placeholder="Email"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
                autoCapitalize="none"
                autoComplete="off"
                autoCorrect={false}
                value={loginData.username}
                onChangeText={newInput =>
                  setLoginData({...loginData, username: newInput})
                }
              />
              <TextInput
                placeholder="Password"
                placeholderTextColor="#C4C4C4"
                style={styles.textInput}
                autoCapitalize="none"
                autoComplete="off"
                autoCorrect={false}
                secureTextEntry={true}
                value={loginData.password}
                onChangeText={newInput =>
                  setLoginData({...loginData, password: newInput})
                }
              />
              <View style={styles.bottomContainer}>
                <TouchableOpacity>
                  <Text style={styles.forgotPassword}>Forgot Password ?</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={login}>
                  <Text style={styles.signIn}>Sign In</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  imageBackground: {
    height: '100%',
    width: '100%',
  },
  imageStyle: {
    width: '100%',
    height: '55%',
    resizeMode: 'cover',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingHorizontal: 30,
  },
  welcomeText: {
    fontSize: 48,
    color: '#FFFFFF',
    fontWeight: '400',
  },
  loginContainer: {
    backgroundColor: '#FFFFFF',
    padding: 25,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '48%',
  },
  loginContainerKeyboardShow: {
    backgroundColor: '#FFFFFF',
    padding: 25,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    width: '100%',
    height: '48%',
  },
  loginTitle: {
    fontSize: 36,
    color: '#18203A',
    fontWeight: '600',
    marginTop: 15,
    marginBottom: 25,
  },
  formContainer: {
    justifyContent: 'space-between',
    height: '100%',
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#828282',
    paddingVertical: 5,
    marginTop: 5,
    marginBottom: 30,
  },
  bottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  forgotPassword: {
    fontSize: 15,
    color: '#FD4D4D',
    fontWeight: '600',
  },
  signIn: {
    fontSize: 15,
    color: '#231F20',
    fontWeight: '600',
  },
});
