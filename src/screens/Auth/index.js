import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import React from 'react';
import ButtonPrimary from '../../components/ButtonPrimary';
import ButtonSecondary from '../../components/ButtonSecondary';

const FirstScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <ImageBackground
          source={require('../../../assets/images/welcome.png')}
          style={styles.imageBackground}
          imageStyle={styles.imageStyle}>
          <View style={styles.titleContainer}>
            <Text style={styles.welcomeText}>Welcome</Text>
          </View>
          <View style={styles.buttonContainer}>
            <ButtonPrimary
              title="Register"
              onPress={() => navigation.navigate('Register')}
            />
            <View style={styles.spacer} />
            <ButtonSecondary
              title="Login"
              onPress={() => navigation.navigate('Login')}
            />
          </View>
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
};

export default FirstScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  imageBackground: {
    height: '100%',
    width: '100%',
  },
  imageStyle: {
    width: '100%',
    height: '86%',
    resizeMode: 'cover',
  },
  titleContainer: {
    height: '30%',
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingHorizontal: 30,
  },
  welcomeText: {
    fontSize: 48,
    color: '#FFFFFF',
    fontWeight: '400',
  },
  buttonContainer: {
    backgroundColor: '#FFFFFF',
    padding: 20,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  spacer: {
    height: 10,
  },
});
