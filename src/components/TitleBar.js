import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';

const TitleBar = ({title, subtitle}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity>
        <Text style={styles.subtitle}>{subtitle}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TitleBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  title: {fontSize: 18, fontWeight: '600', color: '#222B45'},
  subtitle: {fontSize: 13, fontWeight: '400', color: '#8F9BB3'},
});
