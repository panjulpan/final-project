import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';

const ButtonSecondary = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonSecondary;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    paddingVertical: 10,
    borderRadius: 15,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#FD4D4D',
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    color: '#FD4D4D',
  },
});
