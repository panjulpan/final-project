import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';

const ButtonProfile = ({title, subtitle}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
    </TouchableOpacity>
  );
};

export default ButtonProfile;

const styles = StyleSheet.create({
  container: {
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 7,
  },
  title: {
    fontSize: 14,
    fontWeight: '400',
    color: '#222B45',
    marginBottom: 3,
  },
  subtitle: {fontSize: 14, fontWeight: '400', color: '#828282'},
});
