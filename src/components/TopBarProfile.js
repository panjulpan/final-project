import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import IconTwo from 'react-native-vector-icons/MaterialCommunityIcons';
import IconThree from 'react-native-vector-icons/FontAwesome';

const TopBarProfile = () => {
  return (
    <View style={styles.topbar}>
      <View style={styles.searchBox}>
        <Text style={styles.text}>My Account</Text>
      </View>
      <View style={styles.topbarMenuContainer}>
        <IconThree name="bell" size={22} color="#18203A" />
        <IconTwo name="message-processing" size={22} color="#18203A" />
        <IconThree name="gear" size={22} color="#18203A" />
      </View>
    </View>
  );
};

export default TopBarProfile;

const styles = StyleSheet.create({
  topbar: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 15,
    width: '100%',
    backgroundColor: '#FFFFFF',
  },
  searchBox: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    borderRadius: 5,
    flex: 0.7,
  },
  text: {
    fontSize: 24,
    fontWeight: '700',
    color: '#222B45',
  },
  topbarMenuContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.3,
    justifyContent: 'space-around',
  },
});
