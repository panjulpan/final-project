import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';

const ButtonPrimary = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonPrimary;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FD4D4D',
    width: '100%',
    paddingVertical: 10,
    borderRadius: 15,
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    color: '#FFFFFF',
  },
});
