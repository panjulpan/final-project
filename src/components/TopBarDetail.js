import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import IconTwo from 'react-native-vector-icons/FontAwesome';

const TopBarDetail = ({onPress}) => {
  return (
    <View style={styles.container}>
      <View style={{flex: 0.8}}>
        <TouchableOpacity onPress={onPress}>
          <Icon name="arrow-left" color="#FD4D4D" size={28} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: 'row',
          flex: 0.2,
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity>
          <IconTwo name="shopping-cart" color="#FD4D4D" size={28} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Icon name="more-vertical" color="#FD4D4D" size={28} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TopBarDetail;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingHorizontal: 15,
    paddingVertical: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
