import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const Menu = ({color, title, icon}) => {
  return (
    <View style={styles.buttonContainer}>
      <TouchableOpacity style={styles.button}>
        <Icon name={icon} size={32} color={color} />
      </TouchableOpacity>
      <Text style={styles.buttonText}>{title}</Text>
    </View>
  );
};

export default Menu;

const styles = StyleSheet.create({
  buttonContainer: {
    width: 70,
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#F4F4F4',
    borderRadius: 10,
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 7,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 12,
    color: '#FFFFFF',
    fontWeight: '600',
    lineHeight: 12,
  },
});
