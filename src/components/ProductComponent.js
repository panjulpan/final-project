import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';

const ProductComponent = ({item, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image source={{uri: item.image}} style={styles.imageProduct} />
      <View style={{paddingHorizontal: 10, paddingTop: 10}}>
        <Text numberOfLines={1} style={styles.productName}>
          {item.title}
        </Text>
        <Text style={styles.productPrice}>$ {item.price}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ProductComponent;

const styles = StyleSheet.create({
  container: {
    width: 112,
    height: 158,
    borderWidth: 1,
    borderColor: '#F2F2F2',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    marginRight: 10,
  },
  imageProduct: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  productName: {
    fontSize: 12,
    color: '#4F4F4F',
    fontWeight: '400',
    marginBottom: 5,
  },
  productPrice: {fontSize: 10, color: '#4F4F4F', fontWeight: '700'},
});
