import {StyleSheet, TextInput, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import IconTwo from 'react-native-vector-icons/MaterialCommunityIcons';
import IconThree from 'react-native-vector-icons/FontAwesome';

const TopBarHome = () => {
  return (
    <View style={styles.topbar}>
      <View style={styles.searchBox}>
        <Icon name="search" size={18} color="#18203A" />
        <TextInput
          placeholder="Searching..."
          placeholderTextColor="#8F9BB3"
          style={styles.textInput}
        />
      </View>
      <View style={styles.topbarMenuContainer}>
        <IconThree name="heart" size={22} color="#18203A" />
        <IconTwo name="message-processing" size={22} color="#18203A" />
        <IconTwo name="cart" size={22} color="#18203A" />
      </View>
    </View>
  );
};

export default TopBarHome;

const styles = StyleSheet.create({
  topbar: {
    position: 'absolute',
    flexDirection: 'row',
    padding: 15,
    width: '100%',
  },
  searchBox: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 10,
    // paddingVertical: 5,
    alignItems: 'center',
    borderRadius: 5,
    flex: 0.7,
    marginRight: 10,
  },
  textInput: {marginLeft: 10},
  topbarMenuContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.3,
    justifyContent: 'space-around',
  },
});
