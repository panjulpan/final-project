import {View, Text, Dimensions, Platform} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home/Home';
import Icon from 'react-native-vector-icons/Feather';
import FeedScreen from '../screens/Feed/FeedScreen';
import GiftScreen from '../screens/Gift/GiftScreen';
import NotifScreen from '../screens/Notification/NotifScreen';
import ProfileScreen from '../screens/Profile/ProfileScreen';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          paddingTop: 10,
          // paddingBottom: 7,
          height:
            Platform.OS === 'ios'
              ? Dimensions.get('window').height < 737
                ? 60
                : 85
              : 60,
        },
        tabBarActiveTintColor: '#FF3D71',
        tabBarHideOnKeyboard: true,
      }}>
      <Tab.Screen
        name="Discover"
        component={Home}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, size}) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Feed"
        component={FeedScreen}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, size}) => (
            <Icon name="list" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="For You"
        component={GiftScreen}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, size}) => (
            <Icon name="gift" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={NotifScreen}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, size}) => (
            <Icon name="bell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, size}) => (
            <Icon name="user" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
