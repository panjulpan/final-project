import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import FirstScreen from '../screens/Auth';
import Login from '../screens/Auth/Login';
import Register from '../screens/Auth/Register';
import TabNavigator from './TabNavigator';
import DetailProductScreen from '../screens/Home/DetailProductScreen';

const Stack = createNativeStackNavigator();

const StackNavigator = ({tokenLogin}) => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {tokenLogin === null ? (
        <>
          <Stack.Screen name="First" component={FirstScreen} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
        </>
      ) : (
        <>
          <Stack.Screen name="HomePage" component={TabNavigator} />
          <Stack.Screen name="Detail" component={DetailProductScreen} />
        </>
      )}
    </Stack.Navigator>
  );
};

export default StackNavigator;
