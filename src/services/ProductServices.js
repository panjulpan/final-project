import axios from 'axios';

export async function GetProductByLimit() {
  const url = 'https://fakestoreapi.com/products?limit=8';
  try {
    const req = await axios.get(url);

    console.log('get product data: ', req);
    return req;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      const errors = {
        data: error.response.data,
        status: error.response.status,
      };

      return errors;
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
      return error.request;
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
      return error.message;
    }
  }
}
